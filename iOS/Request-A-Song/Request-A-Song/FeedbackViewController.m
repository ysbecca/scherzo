//
//  FirstViewController.m
//  Request-A-Song
//
//  Created by Lineker Tomazeli on 12-01-05.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FeedbackViewController.h"
#import "SocketJSONParser.h"
#import <QuartzCore/QuartzCore.h>

@implementation FeedbackViewController
@synthesize messageBox;
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[self initNetworkCommunication];
	// Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(keyboardWillShow:) 
                                                 name:UIKeyboardWillShowNotification 
                                               object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(keyboardWillHide:) 
                                                 name:UIKeyboardWillHideNotification 
                                               object:nil];
    
	//set notification for when a key is pressed.
	[[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector: @selector(keyPressed:) 
                                                 name: UITextViewTextDidChangeNotification 
                                               object: nil];
    
	//turn off scrolling and set the font details.
	messageBox.scrollEnabled = YES;
	messageBox.font = [UIFont fontWithName:@"Helvetica" size:14]; 
    
    messageBox.layer.cornerRadius = 5;
    //messageBox.clipsToBounds = YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;

}
-(void) keyboardWillShow:(NSNotification *)note{

}
-(void) keyPressed: (NSNotification*) notification{

}

-(void) keyboardWillHide:(NSNotification *)note{

}
- (IBAction)dismissKeyboard:(id)sender {
    [messageBox resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

-(IBAction)SendFeedback:(id)sender
{
    [self dismissKeyboard:sender];
    
    SocketJSONParser *j = [[SocketJSONParser alloc] init]; 
    
    [j sendFeedback:messageBox.text];
    
    messageBox.text = @"";

    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Feedback" message:@"Thank you for your feedback!" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
    [alert show];
}

@end
