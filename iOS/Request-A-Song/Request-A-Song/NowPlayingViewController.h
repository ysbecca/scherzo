//
//  FeedbackViewController.h
//  Request-A-Song
//
//  Created by Lineker Tomazeli on 12-01-05.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SBJsonParser;
@class SBJsonWriter;

@interface NowPlayingViewController : UITableViewController <NSStreamDelegate, UITableViewDelegate, UITableViewDataSource>
{
    NSInputStream	*inputStream;
	NSOutputStream	*outputStream;
    NSArray *songs;
    
    SBJsonParser *_parser;
    SBJsonWriter *_writer;
    
}
-(IBAction) refresh:(id) sender;

@property (nonatomic, retain) NSInputStream *inputStream;
@property (nonatomic, retain) NSOutputStream *outputStream;

@property (nonatomic, strong) NSArray *songs;

- (void) initNetworkCommunication;
- (void)getPlayingList;
-(void)sendSocketMessage:(NSString*)message;
- (void) messageReceived:(NSString *)message;


@end
